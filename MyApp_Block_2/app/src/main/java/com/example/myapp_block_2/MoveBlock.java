package com.example.myapp_block_2;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.DiscretePathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.view.View;


public class MoveBlock extends View {
    int left, top, right, bottom;
    int min, max, dx;
    Paint paint;
    Path path;
    RadialGradient rg;

    //コンストラクタ生成
    public MoveBlock(Context context) {
        super(context);
        left = 0;
        top = 0;
        right = 0;
        bottom = 0;
        min = 0;
        max = 0;
        dx = 0;
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Paint.Style.FILL);
        paint.setPathEffect(new DiscretePathEffect(10, 5));
    }

    //onDraw()メソッド
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        //パス
        path = new Path();
        path.rewind();
        path.moveTo(left, top + (bottom - top) / 2);
        path.lineTo(left + (right - left) / 4, top);
        path.lineTo(right - (right - left) / 4, top);
        path.lineTo(right, top + (bottom - top) / 3);
        path.lineTo(right, bottom - (bottom - top) / 3);
        path.lineTo(right - (right - left) / 4, bottom);
        path.lineTo(left + (right - left) / 4, bottom);
        path.lineTo(left, bottom - (bottom - top) / 2);
        path.close();

        //グラデーション
        int[] colors = { 0xFF2f4f4f, 0xFF006400, 0xFF66cdaa};
        rg = new RadialGradient((left + right) / 2, (top + bottom) / 2, right - left, colors, null, Shader.TileMode.CLAMP);
        paint.setShader(rg);

        //パスで描く
        canvas.drawPath(path, paint);
    }

    //move()メソッド
    protected void move() {
        left += dx;
        right += dx;
      //範囲を超えないようにする
        if (right >= max) {
            dx = -dx;
        }
        else if (left <= min) {
            dx = -dx;
        }
    }
}
