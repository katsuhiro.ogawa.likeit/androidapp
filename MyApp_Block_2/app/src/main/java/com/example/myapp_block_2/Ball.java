package com.example.myapp_block_2;

import static android.graphics.Paint.ANTI_ALIAS_FLAG;
import android.view.View;
import android.content.Context;
import android.graphics.Paint;
import android.graphics.Color;
import android.graphics.Canvas;
import java.util.Random;


public class Ball extends View {
    int x ,y ,radius;
    int dx, dy;
    Paint paint;
    Random r;

    //コンストラクタで初期値設定
    public Ball(Context context){
        super(context);
        radius = 0;            //ボールの半径
        x = 0;
        y = 0;
        dx = 0;
        dy = 0;
        paint = new Paint(ANTI_ALIAS_FLAG);
        paint.setColor(Color.WHITE);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
    }

    protected void onDraw(Canvas canvas){
        super.onDraw(canvas);
        canvas.drawCircle(x , y , radius ,paint);
    }

    protected void move(){
        this.setVisibility(View.VISIBLE);
        x += dx;
        y += dy;
    }
}
