package com.example.myapp_block_2;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.view.View;


public class Bar extends View {
    int left,top,right,bottom;
    Paint paint;
    int size;
    Path path;

    //コンストラクタで初期値設定
    public Bar(Context context) {
        super(context);
        size = 0;               //barの初期サイズを決める（0：ノーマル、2：スモール、4：Wスモール、-2：ラージ、-4：Wラージ）
        left = 0;               //左端から2/5の位置
        right = 0;              //左端から3/5の位置
        top = 0;                //上端から4/5の位置
        bottom = 0;             //上端から4/5+60の位置
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setPathEffect(new CornerPathEffect(50));
        paint.setColor(Color.WHITE);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);    //FILL_AND_STROKE：塗りつぶし＋枠線　　//FILL：塗りつぶしのみ
    }

    //onDraw()メソッド
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);                           //画面にCanvasを描写

        //パス
        path = new Path();
        path.rewind();
        path.moveTo(left, bottom - (bottom - top) / 3);
        path.lineTo(left, top + (bottom - top) / 3);
        path.lineTo(left + (right - left) / 5, top);
        path.lineTo(right - (right - left) / 5, top);
        path.lineTo(right, top + (bottom - top) / 3);
        path.lineTo(right, bottom - (bottom - top) / 3);
        path.lineTo(right - (right - left) / 3, bottom - (bottom - top) / 5);
        path.lineTo(right - (right - left) / 5, bottom);
        path.lineTo(left + (right - left) / 5, bottom);
        path.lineTo(left + (right - left) / 3, bottom - (bottom - top) / 5);
        path.close();
        //パスで描く
        canvas.drawPath(path, paint);
    }

    protected void move(boolean barFlag, int tapx, int width){
        if (barFlag) {
            left = tapx - (width / (10 + size));
            right = tapx + (width / (10 + size));
        }
        else {
        }
    }
}
