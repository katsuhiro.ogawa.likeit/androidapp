package com.example.myapp_block_2;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.graphics.RectF;
import android.graphics.Shader;
import android.view.View;


public class Warp extends View {
    int left, top, right, bottom;
    Paint paint;
    RectF rectf;
    RadialGradient rg;

    //コンストラクタ生成
    public Warp(Context context) {
        super(context);
        left = 0;
        top = 0;
        right = 0;
        bottom = 0;
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setAlpha(210);        //半透明描写(0で完全透明、255で無透明)
    }

    //onDraw()メソッド
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //グラデーション
        int[] colors = { 0xFFd8bfd8, 0xFF000000, 0xFF483d8b};
        rg = new RadialGradient((left + right) / 2, (top + bottom) / 2, right - left, colors, null, Shader.TileMode.CLAMP);
        paint.setShader(rg);

        //円を描く
        rectf = new RectF(left, top, right, bottom);
        canvas.drawOval(rectf, paint);
    }
}
