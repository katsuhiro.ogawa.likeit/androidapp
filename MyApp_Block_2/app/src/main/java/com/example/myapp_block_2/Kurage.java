package com.example.myapp_block_2;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.view.View;
import java.util.Random;


public class Kurage extends View {
    int left, top, right, bottom;
    int minX, maxX, minY, maxY, dx, dy;
    float alpha;
    boolean isVisible;
    Random r;
    Paint paint;
    Path path1, path2;
    RadialGradient rg;

    //コンストラクタ生成
    public Kurage(Context context) {
        super(context);
        left = 0;
        top = 0;
        right = 0;
        bottom = 0;
        minX = 0;
        maxX = 0;
        minY = 0;
        maxY = 0;
        dx = 0;
        dy = 0;
        alpha = 0.6f;
        isVisible = false;
        this.setVisibility(INVISIBLE);
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Paint.Style.FILL);
        paint.setPathEffect(new CornerPathEffect(50));
        paint.setAlpha(180);        //半透明描写(0で完全透明、255で無透明)
    }

    //onDraw()メソッド
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        //パス
        path1 = new Path();
        path1.rewind();
        path1.moveTo(left + (right - left) / 4, top);
        path1.lineTo(right - (right - left) / 4, top);
        path1.lineTo(right, top + (bottom - top) / 2);
        path1.lineTo(right - (right - left) / 4, top + (bottom - top) / 2);
        path1.lineTo(right, bottom - (bottom - top) / 2);
        path1.lineTo(right - (right - left) / 5, top + (bottom - top) / 2);
        path1.lineTo(right - (right - left) / 5, bottom);
        path1.lineTo(right - (right - left) / 4, bottom);
        path1.lineTo(right - (right - left) / 4, top + (bottom - top) / 2);
        path1.lineTo(right - (right - left) / 3, top + (bottom - top) / 2);
        path1.lineTo(right - (right - left) / 3, bottom);
        path1.lineTo(right - (right - left) * 2 / 5, bottom);
        path1.lineTo(right - (right - left) * 2 / 5, top + (bottom - top) / 2);
        path1.lineTo(left + (right - left) * 2 / 5, top + (bottom - top) / 2);
        path1.lineTo(left + (right - left) * 2 / 5, bottom);
        path1.lineTo(left + (right - left) / 3, bottom);
        path1.lineTo(left + (right - left) / 3, top + (bottom - top) / 2);
        path1.lineTo(left + (right - left) / 4, bottom);
        path1.lineTo(left + (right - left) / 4, top + (bottom - top) / 2);
        path1.lineTo(left + (right - left) / 5, bottom);
        path1.lineTo(left + (right - left) / 5, top + (bottom - top) / 2);
        path1.lineTo(left, bottom - (bottom - top) / 2);
        path1.lineTo(left + (right - left) / 4, top + (bottom - top) / 2);
        path1.lineTo(left, top + (bottom - top) / 2);
        path1.close();

        //パス（貝殻）
        path2 = new Path();
        path2.rewind();
        path2.moveTo(left, top + (bottom - top) / 3);
        path2.lineTo(left + (right - left) / 6, top);
        path2.lineTo(right - (right - left) / 6, top);
        path2.lineTo(right, top + (bottom - top) / 3);
        path2.lineTo(right - (right - left) / 6, top + (bottom - top) / 2);
        path2.lineTo(right - (right - left) / 6, bottom);
        path2.lineTo(right - (right - left) / 5, bottom);
        path2.lineTo(right - (right - left) / 5, top + (bottom - top) / 2);
        path2.lineTo(right - (right - left) / 3, top + (bottom - top) / 2);
        path2.lineTo(right - (right - left) / 3, bottom);
        path2.lineTo(right - (right - left) * 2 / 5, bottom);
        path2.lineTo(right - (right - left) * 2 / 5, top + (bottom - top) / 2);
        path2.lineTo(right - (right - left) / 2, bottom);
        path2.lineTo(right - (right - left) / 2, top + (bottom - top) / 2);
        path2.lineTo(left + (right - left) * 2 / 5, top + (bottom - top) / 2);
        path2.lineTo(left + (right - left) * 2 / 5, bottom);
        path2.lineTo(left + (right - left) / 3, bottom);
        path2.lineTo(left + (right - left) / 3, top + (bottom - top) / 2);
        path2.lineTo(left + (right - left) / 4, bottom);
        path2.lineTo(left + (right - left) / 4, top + (bottom - top) / 2);
        path2.lineTo(left + (right - left) / 5, bottom);
        path2.lineTo(left + (right - left) / 5, top + (bottom - top) / 2);
        path2.close();

        //グラデーション
        int[] colors = { 0xFFe6ffe9, 0xFFfafad2, 0xFFffffff};
        rg = new RadialGradient((left + right) / 2, (top + bottom) / 2, (right - left) / 2, colors, null, Shader.TileMode.CLAMP);
        paint.setShader(rg);

        //パスで描く
        canvas.drawPath(path1, paint);
        canvas.drawPath(path2, paint);
    }

    //move()メソッド
    protected void move() {
        left += dx;
        right += dx;
        top += dy;
        bottom += dy;

        r = new Random();
        int random1 = r.nextInt(500);
      //範囲内なら
        if (left > minX && right < maxX && top > minY && bottom < maxY) {
            if (random1 == 1) {
                dx = -dx;
            }
            if (random1 == 2) {
                dy = -dy;
            }
            //ランダムで見えるようにする
            if (random1 == 3) {
                if (alpha < 1.0f) {
                    alpha += 0.1f;
                    this.setAlpha(alpha);
                    if (alpha >= 0.7f) {
                        this.setVisibility(VISIBLE);
                        isVisible = true;
                    }
                }
            }
            //ランダムで見えないようにする
            if (random1 == 4) {
                if (alpha > 0.4f) {
                    alpha -= 0.1f;
                    this.setAlpha(alpha);
                    if (alpha < 0.7f) {
                        this.setVisibility(INVISIBLE);
                        isVisible = false;
                    }
                }
            }
        }

      //範囲を超えないようにする
        if (right >= maxX) {
            dx = -dx;
        }
        else if (left <= minX) {
            dx = -dx;
        }

        if (bottom >= maxY) {
            dy = -dy;
        }
        else if (top <= minY) {
            dy = -dy;
        }
    }
}
