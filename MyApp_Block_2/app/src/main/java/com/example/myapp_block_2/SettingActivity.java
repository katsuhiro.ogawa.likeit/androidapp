package com.example.myapp_block_2;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;


public class SettingActivity extends AppCompatActivity {
    Button bt1Click, bt2Click;
    ImageButton bt3Click;
    TextView textView1;
    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;
    DialogFragment deleteDialog, volumeDialog;      //カスタムダイアログ(フラグメントクラスを別途作成済み)
    AlertDialog.Builder completeDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

    //ナビゲーションバーとステータスバーを非表示にする
        View decor = getWindow().getDecorView();
        // hide navigation bar, show status bar
        decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        // show navigation bar, hide status bar
        decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);

    //ボタンを用意
        bt1Click = findViewById(R.id.setBtn1);
        bt2Click = findViewById(R.id.setBtn2);
        bt3Click = findViewById(R.id.homeBtn);
    //ボタンにリスナーを設定
        StageListener listener = new StageListener();
        bt1Click.setOnClickListener(listener);
        bt2Click.setOnClickListener(listener);
        bt3Click.setOnClickListener(listener);
    //テキストを用意
        textView1 = findViewById(R.id.setting);
    //フォントを取得し、ボタン・テキストに設定（フォントデータはassetsファイルに入っている）
        Typeface customFont1 = Typeface.createFromAsset(getAssets(), "LibreBaskerville-Regular.ttf");
        bt1Click.setTypeface(customFont1);
        bt2Click.setTypeface(customFont1);
        textView1.setTypeface(customFont1);

    //共有プリファレンスを用意(データを削除)
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

    //ダイアログを用意(アラートを表示)
        //yes,no選択画面
        deleteDialog = new DeleteDialogFragment();
        //削除完了画面
        completeDialog = new AlertDialog.Builder(this);
        //ボリューム調整画面
        volumeDialog = new VolumeDialogFragment();
    }

    private class StageListener implements View.OnClickListener{
        @Override
        public void onClick(View view){
            int id = view.getId();
            switch(id){
                case R.id.setBtn1: {
                  //アラートを表示
                    volumeDialog.show(getSupportFragmentManager(), "volume");
                    break;
                }

                case R.id.setBtn2: {
                  //アラートを表示
                    deleteDialog.show(getSupportFragmentManager(), "delete");
                    break;
                }

                case R.id.homeBtn: {
                    Intent intent = new Intent(SettingActivity.this, StartActivity.class);
                    startActivity(intent);
                    break;
                }

            }
        }
    }

    //YESが押された場合の処理
    public void positive(View view) {
      //データ削除
        editor = sharedPref.edit();
        editor.clear();
        editor.apply();
      //ダイアログ非表示
        deleteDialog.dismiss();
      //削除後に完了メッセージをダイアログで表示
        completeDialog.setMessage("The data has been deleted.");
        completeDialog.show();
    }

    //NOが押された場合の処理
    public void negative(View view) {
      //ダイアログ非表示
        deleteDialog.dismiss();
    }

}