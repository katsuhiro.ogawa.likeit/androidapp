package com.example.myapp_block_2;

import static android.util.TypedValue.COMPLEX_UNIT_PX;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class MainActivity extends AppCompatActivity {     //AppCompatActivityを継承することでActivityにActionBarを追加することができる
    //フィールド（変数）一覧
    TextView[] textView;
    Block[][] block;                    //ブロックの配列変数
    MoveBlock[] moveBlock;              //動く反射ブロックの配列変数
    Bar bar;
    List<Ball> balls, keepBalls;        //keepBalls：ボールサーバー内にキープされているボール(ボールを二つのリスト配列に所属させる)
    Smoke smoke;
    Item[][] item;
    Warp[] warp;
    Moving[] moving;
    BallServer server;
    Kurage[] kurage;
    Random r, r1, r2, r3, r4, r5, r6;
    RelativeLayout relativeLayout;
    RelativeLayout.LayoutParams params;
    SharedPreferences sharedPref;           //共有プリファレンスの変数を用意(クリアデータが格納されている)
    int stageNum;                                                           //クリアごとにステージを変える
    int width, height, tapx, WC = ViewGroup.LayoutParams.WRAP_CONTENT;      //width,height：端末の高さ,幅　 tap：タップした時のx座標
    int breakBlocks, countBar, ballNum, ballCount;                          //breakBlocks：壊したブロックの数     //count：繰り返し処理(runnable)内での回数制限用    //ballCount：画面上のボールの数    //ballNum：増加するボールの数
    boolean barFlag;                                                        //barFlag：バーが動けるかどうか
    int barRangeY, serverRangeX, serverRangeY, blockRangeX, blockRangeY, moveBlockRangeX, moveBlockRangeY;       //判定の境界線範囲
    boolean gameGamenFlag, gameStartGamenFlag;       //ゲーム画面にいるかどうか、ゲームスタート画面にいるかどうか
    boolean isBreak = false;
    Handler handler, handler1, handler2;        //繰り返し処理用
    Runnable runnable;                          //繰り返し処理用
    TranslateAnimation translateAnimation;
    AlphaAnimation alphaAnimation;
    Window window;
    Intent intent;
    InputStream inputStream, inputStream2;
    BufferedReader bufferedReader, bufferedReader2;
    StringBuilder stringBuilder, stringBuilder2;
    String str, str2;
    Pattern pattern, pattern1, pattern2;
    Matcher matcher, matcher1, matcher2;
    List<String> list, list1, list2;
    //(ステージ毎の設定)
    //テキスト
    int textNum;
    String textViewProperty1, textViewProperty2;
    String text0, text1, text2, text3, text4, text5, text6, text7;
    //ブロック
    int colSize, rowSize;
    int margin;
    String blockType;
    //動く反射ブロック
    int moveBlockNum;
    String moveBlockProperty1, moveBlockProperty2, moveBlockProperty3, moveBlockProperty4, moveBlockProperty5,
            moveBlockProperty6, moveBlockProperty7, moveBlockProperty8;
    //ワープ
    int warpNum;
    String warpProperty1, warpProperty2, warpProperty3, warpProperty4;
    //ムービング
    int movingNum;
    String movingProperty1, movingProperty2, movingProperty3, movingProperty4;
    //ボールサーバー
    String serverProperty1, serverProperty2;
    //クラゲ
    int kurageNum;
    String kurageProperty1, kurageProperty2, kurageProperty3, kurageProperty4, kurageProperty5,
            kurageProperty6, kurageProperty7, kurageProperty8;


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {        //オブジェクト生成(ゲームに必要なクラスをnewする、ビューやボタン生成など)    //onDestroy()と対
        super.onCreate(savedInstanceState);

    //ナビゲーションバーとステータスバーを非表示にする(タイトルバー非表示はthemes.xmlにて設定)
        View decor = getWindow().getDecorView();
        // hide navigation bar, show status bar
        decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        // show navigation bar, hide status bar
        decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);

    //レイアウト作成（描画をActivityから行う）
        relativeLayout = new RelativeLayout(this);          //LayoutインスタンスにActivityの情報を渡す
        relativeLayout.setBackgroundColor(Color.parseColor("#20b2aa"));            //背景色設定
        setContentView(relativeLayout);                             //レイアウトを指定：通常はsetContentView(R.layout.activity_main)

    //スクリーンのサイズを取得（Android端末の実機を用いたシステムを利用　他.加速度センサーなど）
        WindowManager windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);     //getSystemServiceメソッドでWINDOW_SERVICEを指定してwindowManagerオブジェクトを取得
        Display display = windowManager.getDefaultDisplay();                                //取得したwindowManagerのgetDefaultDisplay()メソッドを使えば、その端末情報を取得できる。
        Point point = new Point();          //Pointクラス：整数精度で指定される(x,y)座標空間での位置を表す点（フィールドにx座標、y座標といった情報をもつ）　//Point()メソッド：座標空間の原点 (0, 0) に点を構築して初期化します。
        display.getSize(point);             //アプリケーションの領域を取得（座標情報で入手）
        width = point.x;
        height = point.y;

    //ステージ番号を入手する(StartActivityからintentで入手)
        intent = this.getIntent();
        stageNum = intent.getIntExtra("stageNum",0);

    //共有プリファレンスを用意(クリアデータを格納)
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

    //初期化プロセス
        gameProcess('i');
    }


    //プロセス移行メソッド
    @RequiresApi(api = Build.VERSION_CODES.O)
    public void gameProcess(char s){
        switch (s){
            //初期処理(init)
            case 'i': {
                //初期値設定
                runnable = null;        //これがないとタッチしていないのに勝手に動く
                breakBlocks = 0;
                ballNum = 7;
                ballCount = 0;
                countBar = 0;
                barFlag = true;
                gameGamenFlag = false;
                gameStartGamenFlag = false;
                barRangeY = height / 456;           //barRangeY = 5
                serverRangeX = width / 216;     //ballServerRangeX = 5
                serverRangeY = height / 456;    //ballServerRangeY = 5
                blockRangeX = width / 216;          //blockRangeX = 5
                blockRangeY = height / 456;         //blockRangeY = 5
                moveBlockRangeY = height / 456;     //moveBlockRangeY = 5

                //オブジェクト生成&描写
                objectsCreate();

                //テキストにリスナーを設定
                //SELECTボタン
                textView[2].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        relativeLayout.removeAllViews();    //描画を全削除
                        intent = new Intent(MainActivity.this, SelectActivity.class);
                        startActivity(intent);
                    }
                });
                //RETRYボタン
                textView[3].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        relativeLayout.removeAllViews();    //描画を全削除
                        objectsDelete();                    //オブジェクトにnull格納
                        //初期化
                        gameProcess('i');
                    }
                });
                //NEXTボタン
                textView[4].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        relativeLayout.removeAllViews();    //描画を全削除
                        objectsDelete();                    //オブジェクトにnull格納
                        //初期化
                        stageNum++;
                        gameProcess('i');
                    }
                });

                //繰り返し処理の準備
                handler = new Handler();
                handler1 = new Handler(getMainLooper());
                handler2 = new Handler(getMainLooper());

                //ステージ名表示
                String text = text6 + stageNum;
                if (stageNum < 10) {                //ステージ番号が1桁ならスペース入れる
                    text = text6 + " " + stageNum;
                }
                textView[6].setText(text);

                //タッチイベント無効化するクラスを呼び出す
                window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                //初期アニメーション
                objectsAppear(1);             //オブジェクト画面IN
                /* ステージ名が表示された5秒後(アニメーションにかかる時間)にタッチイベント無効化解除 */
                handler1.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                    //ステージ名をアニメーションで消す
                        startAnimation(textView[6], height, 2);
                    }
                }, 2000);
                handler2.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                    //タッチイベント無効化解除
                        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    //ゲームスタート画面フラグ、ゲーム画面フラグをON
                        gameStartGamenFlag = true;
                        gameGamenFlag = true;
                        textView[0].setText(text0);                     //スタート表示
                        startAnimation(textView[0], height, 3);    //強調アニメーション
                    }
                }, 5000);
            }
            break;


            //GAMEOVER処理
            case 'o': {
                //ゲーム画面フラグをOFF
                gameGamenFlag = false;

                //スモークを非表示
                smoke.setVisibility(View.INVISIBLE);

                //クラゲを非表示
                for (int num = 0; num < kurageNum; num++) {
                    kurage[num].setVisibility(View.INVISIBLE);
                }

                //タッチイベント無効化
                window.addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                handler2.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //タッチイベント無効化解除
                        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    }
                }, 5000);

                //オブジェクトを非表示
                runnable = null;                    //描画を停止
                relativeLayout.removeAllViews();    //handlerがダブってしまうので前の描画を削除

                //オブジェクト画面OUT
                objectsAppear(0);

                //テキストを表示
                relativeLayout.addView(textView[1]);
                relativeLayout.addView(textView[2]);
                relativeLayout.addView(textView[3]);

                //テキストをアニメーション表示
                startAnimation(textView[1], height, 1);     //テキスト画面IN
                startAnimation(textView[2], height, 1);
                startAnimation(textView[3], height, 1);
                textView[1].setText(text1);
                textView[2].setText(text2);
                textView[3].setText(text3);
            }
            break;


            //GAMECLEAR処理
            case 'c': {
                //ゲーム画面フラグをOFF
                gameGamenFlag = false;

                //共有プリファレンスにクリアデータ保存
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt("" + stageNum, 1);       //キー：ステージ番号、値：1
                editor.apply();

                //スモークを非表示
                smoke.setVisibility(View.INVISIBLE);

                //クラゲを非表示
                for (int num = 0; num < kurageNum; num++) {
                    kurage[num].setVisibility(View.INVISIBLE);
                }

                //タッチイベント無効化
                window.addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                handler2.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //タッチイベント無効化解除
                        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    }
                }, 5000);

                //オブジェクトを非表示
                runnable = null;                    //描画を停止
                relativeLayout.removeAllViews();    //handlerがダブってしまうので前の描画を削除

                //オブジェクト画面OUT
                objectsAppear(0);

                //テキストを表示
                relativeLayout.addView(textView[2]);
                relativeLayout.addView(textView[4]);
                relativeLayout.addView(textView[5]);

                //テキストをアニメーション表示
                startAnimation(textView[2], height, 1);     //テキスト画面IN
                startAnimation(textView[4], height, 1);
                startAnimation(textView[5], height, 1);
                textView[2].setText(text2);
                textView[4].setText(text4);
                textView[5].setText(text5);
            }
            break;
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public void objectsCreate() {
        //テキスト変数（assetsから"text"の配置ファイルを取得）
        inputStream = null;
        bufferedReader = null;
        stringBuilder = null;
        try {
            inputStream = getAssets().open("text");                       // assetsから"text"ファイルを取得する
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));    // バッファリング(データを一定数溜めてから入力処理)する
            stringBuilder = new StringBuilder();
            while ((str = bufferedReader.readLine()) != null) {                 // 1行読み込みを繰り返す
                stringBuilder.append(str);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (inputStream != null) inputStream.close();
                if (bufferedReader != null) bufferedReader.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (stringBuilder != null) {
            pattern = Pattern.compile("\".+?\"");                       // ""で挟まれた文字列を抽出する。
            matcher = pattern.matcher(stringBuilder.toString());
            list = new ArrayList<String>();
            while (matcher.find()) {
                list.add(matcher.group());
            }
            textNum = Integer.parseInt(list.get(0).substring(1, list.get(0).length()-1));
            textViewProperty1 = list.get(1).substring(1, list.get(1).length()-1);
            textViewProperty2 = list.get(2).substring(1, list.get(2).length()-1);
            text0 = list.get(3).substring(1, list.get(3).length()-1);
            text1 = list.get(4).substring(1, list.get(4).length()-1);
            text2 = list.get(5).substring(1, list.get(5).length()-1);
            text3 = list.get(6).substring(1, list.get(6).length()-1);
            text4 = list.get(7).substring(1, list.get(7).length()-1);
            text5 = list.get(8).substring(1, list.get(8).length()-1);
            text6 = list.get(9).substring(1, list.get(9).length()-1);
            text7 = list.get(10).substring(1, list.get(10).length()-1);
        }

        //オブジェクト変数（assetsから"stage"の配置ファイルを取得）
        inputStream2 = null;
        bufferedReader2 = null;
        stringBuilder2 = null;
        try {
            inputStream2 = getAssets().open("stage"+stageNum);                    // assetsから"stage"ファイルを取得する
            bufferedReader2 = new BufferedReader(new InputStreamReader(inputStream2));    // バッファリング(データを一定数溜めてから入力処理)する
            stringBuilder2 = new StringBuilder();
            while ((str2 = bufferedReader2.readLine()) != null) {                 // 1行読み込みを繰り返す
                stringBuilder2.append(str2);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (inputStream2 != null) inputStream2.close();
                if (bufferedReader2 != null) bufferedReader2.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (stringBuilder2 != null) {
            pattern1 = Pattern.compile("\".+?\"");                       // ""で挟まれた文字列を抽出する。
            matcher1 = pattern1.matcher(stringBuilder2.toString());
            pattern2 = Pattern.compile("\'.+?\'");
            matcher2 = pattern2.matcher(stringBuilder2.toString());
            list1 = new ArrayList<String>();
            list2 = new ArrayList<String>();
            while (matcher1.find()) {
                list1.add(matcher1.group());
            }
            while (matcher2.find()) {
                list2.add(matcher2.group());
            }
            //ブロック
            colSize = Integer.parseInt(list1.get(0).substring(1, list1.get(0).length()-1));
            rowSize = Integer.parseInt(list1.get(1).substring(1, list1.get(1).length()-1));
            margin = width * Integer.parseInt(list1.get(2).substring(1, list1.get(2).length()-1)) / 100;
            blockType = String.join("", list2).replace("'", "");
            //動く反射ブロック
            moveBlockNum = Integer.parseInt(list1.get(3).substring(1, list1.get(3).length()-1));
            moveBlockProperty1 = list1.get(4).substring(1, list1.get(4).length()-1);
            moveBlockProperty2 = list1.get(5).substring(1, list1.get(5).length()-1);
            moveBlockProperty3 = list1.get(6).substring(1, list1.get(6).length()-1);
            moveBlockProperty4 = list1.get(7).substring(1, list1.get(7).length()-1);
            //動作プロパティ
            moveBlockProperty5 = list1.get(8).substring(1, list1.get(8).length()-1);
            moveBlockProperty6 = list1.get(9).substring(1, list1.get(9).length()-1);
            moveBlockProperty7 = list1.get(10).substring(1, list1.get(10).length()-1);
            moveBlockProperty8 = list1.get(11).substring(1, list1.get(11).length()-1);
            //ワープ
            warpNum = Integer.parseInt(list1.get(12).substring(1, list1.get(12).length()-1));
            warpProperty1 = list1.get(13).substring(1, list1.get(13).length()-1);
            warpProperty2 = list1.get(14).substring(1, list1.get(14).length()-1);
            warpProperty3 = list1.get(15).substring(1, list1.get(15).length()-1);
            warpProperty4 = list1.get(16).substring(1, list1.get(16).length()-1);
            //ムービング
            movingNum = Integer.parseInt(list1.get(17).substring(1, list1.get(17).length()-1));
            movingProperty1 = list1.get(18).substring(1, list1.get(18).length()-1);
            movingProperty2 = list1.get(19).substring(1, list1.get(19).length()-1);
            movingProperty3 = list1.get(20).substring(1, list1.get(20).length()-1);
            movingProperty4 = list1.get(21).substring(1, list1.get(21).length()-1);
            //ボールサーバー
            serverProperty1 = list1.get(22).substring(1, list1.get(22).length()-1);
            serverProperty2 = list1.get(23).substring(1, list1.get(23).length()-1);
            //クラゲ
            kurageNum = Integer.parseInt(list1.get(24).substring(1, list1.get(24).length()-1));
            kurageProperty1 = list1.get(25).substring(1, list1.get(25).length()-1);
            kurageProperty2 = list1.get(26).substring(1, list1.get(26).length()-1);
            kurageProperty3 = list1.get(27).substring(1, list1.get(27).length()-1);
            kurageProperty4 = list1.get(28).substring(1, list1.get(28).length()-1);
            kurageProperty5 = list1.get(29).substring(1, list1.get(29).length()-1);
            kurageProperty6 = list1.get(30).substring(1, list1.get(30).length()-1);
            kurageProperty7 = list1.get(31).substring(1, list1.get(31).length()-1);
            kurageProperty8 = list1.get(32).substring(1, list1.get(32).length()-1);
        }

        //オブジェクトを生成
        //Barを生成
        bar = new Bar(MainActivity.this);
        //初期配置
        bar.size = 0;
        bar.left = width / 5 * 2;               //左端から2/5の位置
        bar.right = width / 5 * 3;              //左端から3/5の位置
        bar.top = height / 5 * 4;               //上端から4/5の位置
        bar.bottom = height / 5 * 4 + height / 38;       //上端から4/5+60の位置
        relativeLayout.addView(bar);

        //Smokeを生成
        smoke = new Smoke(MainActivity.this);
        //初期配置
        smoke.top = -height / 2;
        smoke.right = width;
        smoke.dy = height / 228;        //smoke.dy = 10
        relativeLayout.addView(smoke);

        //Blockを生成
        int order = 0;       //order：以下、ブロックタイプ設定メソッド内にて配列を切り出す順序
        block = new Block[colSize][rowSize];        //ブロックの配列を作成し、変数に代入
        //以下で配列要素ごとに処理を記述
        for (int row = 0; row < rowSize; row++) {
            for (int col = 0; col < colSize; col++) {
                block[col][row] = new Block(this);
                //初期配置
                block[col][row].left = (width - (colSize - 1) * margin) / colSize * col + col * margin;
                block[col][row].right = (width - (colSize - 1) * margin) / colSize * (col + 1) + col * margin;
                block[col][row].top = height / 25 * row + (row + 1) * margin;
                block[col][row].bottom = height / 25 * (row + 1) + (row + 1) * margin;
                //プロパティの設定
                String type = blockType.substring(order, order+1);    //各配列の番号(0 or 1)を切り出す
                if (type.equals("0")) {
                    block[col][row].top = -height / 46;             //block[col][row].top = -50
                    block[col][row].bottom = -height / 46;          //block[col][row].bottom = -50
                    block[col][row].setVisibility(View.GONE);       //ブロック無しのため、ブロックを消す
                    breakBlocks++;
                }
                if (type.equals("1")) {
                    block[col][row].type = 1;
                }
                else if (type.equals("2")) {
                    block[col][row].breakable = false;       //壊れないブロックとする
                    block[col][row].type = 2;
                    breakBlocks++;
                }
                else if (type.equals("3")) {
                    block[col][row].breakable = false;       //壊れないブロックとする(白い貝殻)
                    block[col][row].type = 3;
                    breakBlocks++;
                }
                else if (type.equals("4")) {
                    block[col][row].breakable = false;       //壊れないブロックとする(黄色貝殻)
                    block[col][row].type = 4;
                    breakBlocks++;
                }
                order++;
                relativeLayout.addView(block[col][row]);
            }
        }

        //MoveBlockを生成
        moveBlock = new MoveBlock[moveBlockNum];
        for (int num = 0; num < moveBlockNum; num++) {
            moveBlock[num] = new MoveBlock(this);
            //初期配置
            int property1 = Integer.parseInt(moveBlockProperty1.substring(num*3, (num+1)*3));    //プロパティを切り出す（列番号、3文字）
            int property2 = Integer.parseInt(moveBlockProperty2.substring(num*3, (num+1)*3));    //プロパティを切り出す（ブロックの幅、3文字）
            int property3 = Integer.parseInt(moveBlockProperty3.substring(num*3, (num+1)*3));    //プロパティを切り出す（初期値のx座標の符号、3文字）
            int property4 = Integer.parseInt(moveBlockProperty4.substring(num*3, (num+1)*3));    //プロパティを切り出す（初期値のx座標、3文字）
            moveBlock[num].top = height / 25 * (property1 - 1) + property1 * margin;             //property1：列番号
            moveBlock[num].bottom = height / 25 * property1 + property1 * margin;
            if (property3 == 0){
                moveBlock[num].left = width * property4 / 100;        //property4：初期値（x座標）  property3：その符号（0なら＋、それ以外ならー）
            }
            else {
                moveBlock[num].left = -width * property4 / 100;
            }
            moveBlock[num].right = moveBlock[num].left + width * property2 / 100;         //property2：ブロックの幅
            //動作プロパティを設定
            int property5 = Integer.parseInt(moveBlockProperty5.substring(num * 3, (num + 1) * 3));    //プロパティを切り出す（ブロックの動く範囲(Min)の符号、3文字）
            int property6 = Integer.parseInt(moveBlockProperty6.substring(num * 3, (num + 1) * 3));    //プロパティを切り出す（ブロックの動く範囲(Min)、3文字）
            int property7 = Integer.parseInt(moveBlockProperty7.substring(num * 3, (num + 1) * 3));    //プロパティを切り出す（ブロックの動く範囲(Max)、3文字）
            int property8 = Integer.parseInt(moveBlockProperty8.substring(num * 3, (num + 1) * 3));    //プロパティを切り出す（ブロックの初期動作(dx、-dx)、3文字）
            moveBlock[num].min = width * property6 / 100;
            if (property5 != 0)    moveBlock[num].min = -width * property6 / 100;     //property5：符号（0なら＋、それ以外ならー）
            moveBlock[num].max = width * property7 / 100;
            moveBlock[num].dx = width * property8 / 1000;
            relativeLayout.addView(moveBlock[num]);
        }

        //Itemを生成
        item = new Item[colSize][rowSize];
        for (int row2 = 0; row2 < rowSize; row2++) {
            for (int col2 = 0; col2 < colSize; col2++) {
                item[col2][row2] = new Item(this);
                //初期配置(ブロックと同じ)
                item[col2][row2].left = (width - (colSize - 1) * margin) / colSize * col2 + col2 * margin;
                item[col2][row2].right = (width - (colSize - 1) * margin) / colSize * (col2 + 1) + col2 * margin;
                item[col2][row2].top = height / 25 * row2 + row2 * margin;
                item[col2][row2].bottom = height / 25 * (row2 + 1) + row2 * margin;
                //大きさ変更
                item[col2][row2].left = item[col2][row2].left + (item[col2][row2].right - item[col2][row2].left) / 3;
                item[col2][row2].right = item[col2][row2].right - (item[col2][row2].right - item[col2][row2].left) / 3;
                item[col2][row2].top = item[col2][row2].top + (item[col2][row2].bottom - item[col2][row2].top) / 3;
                item[col2][row2].bottom = item[col2][row2].bottom - (item[col2][row2].bottom - item[col2][row2].top) / 3;

                item[col2][row2].isVisible = false;
                item[col2][row2].dy = height / 456;         //item[col2][row2].dy = 5
                item[col2][row2].setVisibility(View.INVISIBLE);       //呼ばれるまで非表示
                //ランダムにアイテム生成(アイテムごとに色変更)
                item[col2][row2].r = new Random();
                item[col2][row2].itemRandom = item[col2][row2].r.nextInt(Item.itemNum);
                relativeLayout.addView(item[col2][row2]);
            }
        }

        //Warpを生成
        warp = new Warp[warpNum];
        for (int num = 0; num < warpNum; num++) {
            warp[num] = new Warp(this);
            //初期配置
            int warpWidth = Integer.parseInt(warpProperty1.substring(num*3, (num+1)*3));        //プロパティを切り出す（warpの幅、3文字）
            int warpHeight = Integer.parseInt(warpProperty2.substring(num*3, (num+1)*3));       //プロパティを切り出す（warpの高さ、3文字）
            warp[num].left = width * Integer.parseInt(warpProperty3.substring(num*3, (num+1)*3)) / 100;       //プロパティを切り出す（left、3文字）
            warp[num].top = height * Integer.parseInt(warpProperty4.substring(num*3, (num+1)*3)) / 100;        //プロパティを切り出す（top、3文字）
            warp[num].right = warp[num].left + width * warpWidth / 100;
            warp[num].bottom = warp[num].top + height * warpHeight / 100;
            relativeLayout.addView(warp[num]);
        }

        //Movingを生成
        moving = new Moving[movingNum];
        for (int num = 0; num < movingNum; num++) {
            moving[num] = new Moving(this);
            //初期配置
            int movingWidth = Integer.parseInt(movingProperty1.substring(num*3, (num+1)*3));        //プロパティを切り出す（movingの幅、3文字）
            int movingHeight = Integer.parseInt(movingProperty2.substring(num*3, (num+1)*3));       //プロパティを切り出す（movingの高さ、3文字）
            moving[num].left = width * Integer.parseInt(movingProperty3.substring(num*3, (num+1)*3)) / 100;       //プロパティを切り出す（left、3文字）
            moving[num].top = height * Integer.parseInt(movingProperty4.substring(num*3, (num+1)*3)) / 100;        //プロパティを切り出す（top、3文字）
            moving[num].right = moving[num].left + width * movingWidth / 100;
            moving[num].bottom = moving[num].top + height * movingHeight / 100;
            relativeLayout.addView(moving[num]);
        }

        //BallServerを生成
        server = new BallServer(this);
        //初期配置
        server.left = width * Integer.parseInt(serverProperty1.substring(0, 3)) / 100;       //プロパティを切り出す（left、3文字）
        server.top = height * Integer.parseInt(serverProperty2.substring(0, 3)) / 100;        //プロパティを切り出す（top、3文字）
        server.right = server.left + width / 4;         //幅：width / 4
        server.bottom = server.top + height / 15;       //高さ：height / 15
//        relativeLayout.addView(server);       //これはボールの下に書く（デザイン的な問題）

        //Ballを生成
        balls = new ArrayList<Ball>();
        for (int i = 0; i < ballNum; i++){
            balls.add(new Ball(MainActivity.this));
        }
        for (Ball ball:balls) {
            //初期配置
            r4 = new Random();
            int random = r4.nextInt(20);
            ball.x = server.left + (server.right - server.left) / 3 + (server.right - server.left) / 48 * random;
            ball.y = (server.top + server.bottom) / 2;
            ball.radius = width / 54;       //ball.radius = 20
            ball.dx = width / 108;          //ball.dx = 10
            ball.dy = height / 228;         //ball.dy = 10
            //進む方向をランダムで決める
            ball.dx = (random % 2 * 2 - 1) * ball.dx;
            relativeLayout.addView(ball);
        }
        relativeLayout.addView(server);         //BallServerはBallの上に必ず描かれるようにする
        // BallServerでボールをキープ
        keepBalls = new LinkedList<Ball>();         //ArrayListよりも追加・削除が高速
//        for (int i = 0; i < 5; i++){
//            keepBalls.add(balls.get(i));
//        }

        //Kurageを生成
        kurage = new Kurage[kurageNum];
        for (int num = 0; num < kurageNum; num++) {
            kurage[num] = new Kurage(this);
            int property1 = Integer.parseInt(kurageProperty1.substring(num * 3, (num + 1) * 3));    //プロパティを切り出す（クラゲの幅、3文字）
            int property2 = Integer.parseInt(kurageProperty2.substring(num * 3, (num + 1) * 3));    //プロパティを切り出す（クラゲの高さ、3文字）
            int property3 = Integer.parseInt(kurageProperty3.substring(num * 3, (num + 1) * 3));    //プロパティを切り出す（クラゲの動くx軸範囲(MinX)の符号、3文字）
            int property4 = Integer.parseInt(kurageProperty4.substring(num * 3, (num + 1) * 3));    //プロパティを切り出す（クラゲの動くx軸範囲(MinX)、3文字）
            int property5 = Integer.parseInt(kurageProperty5.substring(num * 3, (num + 1) * 3));    //プロパティを切り出す（クラゲの動くx軸範囲(MaxX)、3文字）
            int property6 = Integer.parseInt(kurageProperty6.substring(num * 3, (num + 1) * 3));    //プロパティを切り出す（クラゲの動くy軸範囲(MinY)の符号、3文字）
            int property7 = Integer.parseInt(kurageProperty7.substring(num * 3, (num + 1) * 3));    //プロパティを切り出す（クラゲの動くy軸範囲(MinY)、3文字）
            int property8 = Integer.parseInt(kurageProperty8.substring(num * 3, (num + 1) * 3));    //プロパティを切り出す（クラゲの動くy軸範囲(MaxY)、3文字）

            r5 = new Random();
            int random = r5.nextInt(100);
         //動作プロパティを設定
           //x軸
            if (property3 == 0)  kurage[num].minX = width * property4 / 100;     //property3：符号（0なら＋、それ以外ならー）
            else kurage[num].minX = -width * property4 / 100;
            kurage[num].maxX = width * property5 / 100;
           //y軸
            if (property6 == 0)  kurage[num].minY = height * property7 / 100;     //property3：符号（0なら＋、それ以外ならー）
            else kurage[num].minY = -height * property7 / 100;
            kurage[num].maxY = height * property8 / 100;
            kurage[num].dx = width / 700;           //kurage.dx = 1
            kurage[num].dy = height / 1200;           //kurage.dy = 1
         //初期配置
          //x軸
            kurage[num].left = kurage[num].minX + (kurage[num].maxX - kurage[num].minX) / 100 * random;   //範囲内のランダムな位置に設定
            kurage[num].right = kurage[num].left + width * property1 / 100;         //property1：クラゲの幅
          //y軸
            kurage[num].top = kurage[num].minY + (kurage[num].maxY - kurage[num].minY) / 100 * random;   //範囲内のランダムな位置に設定
            kurage[num].bottom = kurage[num].top + height * property2 / 100;         //property2：クラゲの高さ
            relativeLayout.addView(kurage[num]);
        }

        //Textを生成
        textView = new TextView[textNum];
        for (int num = 0; num < textNum; num++) {
            textView[num] = new TextView(this);
            textView[num].setText("");
            //プロパティの設定
            int size = width * Integer.parseInt(textViewProperty1.substring(num*3, (num+1)*3)) / 1000;         //プロパティを切り出す（サイズ、3文字）
            int topMargin = height * Integer.parseInt(textViewProperty2.substring(num*3, (num+1)*3)) / 100;    //プロパティを切り出す（topMargin、3文字）
            int leftMargin = 0;
            switch (num) {
                case 0: leftMargin = width/3; break;        //text0 = "Tap to Start"
                case 1: leftMargin = width/11; break;       //text1 = "GAMEOVER"
                case 2: leftMargin = width/11; break;       //text2 = "SELECT"
                case 3: leftMargin = width*7/11; break;     //text3 = "RETRY"
                case 4: leftMargin = width*2/3; break;     //text4 = "NEXT"
                case 5: leftMargin = width/4; break;       //text5 = "CLEAR"
                case 6: leftMargin = width/5; break;        //text6 = "STAGE  "
                case 7: leftMargin = width/3; break;        //text7 = "Tap to Restart"
            }
            textView[num].setTextSize(COMPLEX_UNIT_PX, size);       //テキストサイズをピクセルで設定する
            //レイアウトの属性を設定(LayoutParamsクラス)
            params = new RelativeLayout.LayoutParams(WC, WC);      //テキストの表示形式を設定：LayoutParams(width, height)
            params.addRule(RelativeLayout.ALIGN_PARENT_TOP);                                   //テキストの位置を設定（親の上辺）
            params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);                                  //テキストの位置を設定（親の左辺）
            params.setMargins(leftMargin, topMargin, 0, 0);                        //左、上、右、下のマージン
            //色プロパティ情報を設定
            textView[num].setTextColor(Color.WHITE);
            relativeLayout.addView(textView[num], params);
        }

        //フォントを取得し、テキストに設定
        Typeface customFont1 = Typeface.createFromAsset(getAssets(), "LibreBaskerville-Regular.ttf");
        textView[0].setTypeface(customFont1);       //"タップしてスタート"
        textView[1].setTypeface(customFont1);       //"GAMEOVER"
        textView[2].setTypeface(customFont1);       //"ステージ選択"
        textView[3].setTypeface(customFont1);       //"もう一度トライ"
        textView[4].setTypeface(customFont1);       //"次のステージ"
        textView[5].setTypeface(customFont1);       //"CLEAR"
        textView[6].setTypeface(customFont1);       //"ステージ名"
        textView[7].setTypeface(customFont1);       //"タップして再開"
    }


    public void objectsDelete() {
        //assetsファイル読み込み用
        inputStream = null;
        inputStream2 = null;
        bufferedReader = null;
        bufferedReader2 = null;
        stringBuilder = null;
        stringBuilder2 = null;
        str = null;
        str2 = null;
        pattern = null;
        pattern1 = null;
        pattern2 = null;
        matcher = null;
        matcher1 = null;
        matcher2 = null;
        list = null;
        list1 = null;
        list2 = null;
        //textView
        textView = null;
        params = null;
        //block
        for (int row = 0; row < rowSize; row++) {
            for (int col = 0; col < colSize; col++) {
                block[col][row].paint = null;
                block[col][row].rectf = null;
                block[col][row].rg = null;
            }
        }
        block = null;
        //moveBlock
        for (int num = 0; num < moveBlockNum; num++) {
            moveBlock[num].paint = null;
            moveBlock[num].path = null;
            moveBlock[num].rg = null;
        }
        moveBlock = null;
        //bar
        bar.paint = null;
        bar = null;
        //ball
        for (Ball ball:balls) {
            ball.r = null;
            ball.paint = null;
        }
        balls = null;
        keepBalls = null;
        //smoke
        smoke.paint = null;
        smoke.rect = null;
        smoke.lig = null;
        smoke = null;
        //item
        for (int r = 0; r < rowSize; r++) {
            for (int c = 0; c < colSize; c++) {
                item[c][r].r = null;
                item[c][r].paint = null;
                item[c][r].lig = null;
                item[c][r].rectf = null;
            }
        }
        item = null;
        //warp
        for (int num = 0; num < warpNum; num++) {
            warp[num].paint = null;
            warp[num].rectf = null;
            warp[num].rg = null;
        }
        warp = null;
        //moving
        for (int num = 0; num < movingNum; num++) {
            moving[num].paint = null;
            moving[num].rectf = null;
            moving[num].lig = null;
        }
        moving = null;
        //server
        server.paint = null;
        server.rg = null;
        server.path = null;
        server = null;
        //kurage
        for (int num = 0; num < kurageNum; num++) {
            kurage[num].r = null;
            kurage[num].paint = null;
            kurage[num].path1 = null;
            kurage[num].path2 = null;
            kurage[num].rg = null;
        }
        kurage = null;
        //this
        r = null;
        r1 = null;
        r2 = null;
        r3 = null;
        r4 = null;
        r5 = null;
        r6 = null;
        intent = null;
        window = null;
        handler = null;
        handler1 = null;
        handler2 = null;
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onRestart() {          //onStop()と対
        super.onRestart();
        //タッチされたらゲーム再開
        if (gameGamenFlag && gameStartGamenFlag == false) {                //ゲーム画面だったら表示する
            textView[7].setText(text7);                     //リスタート表示
            startAnimation(textView[7], height, 3);     //強調アニメーション
        }
    }


    @Override
    protected void onStart() {          //初期値設定、処理の前準備として初期化メソッドを呼ぶ(timerクラスをnewする、ボタンにListenerを設定するなど)  //onStop()と対
        super.onStart();
    }


    @Override
    protected void onResume() {         //実際の処理(dx++, timer+1など)    //onPause()と対
        super.onResume();
    }


    @Override
    protected void onPause() {          //onResume()と対
        super.onPause();
        if (gameGamenFlag && gameStartGamenFlag == false) {
            runnable = null;            //ゲーム画面なら一時停止する。画面にもう一度触れると再開
        }
    }


    @Override
    protected void onStop() {           //onStart()と対
        super.onStop();
        intent = null;
    }


    @Override
    protected void onDestroy() {        //onCreate()と対
        super.onDestroy();
        objectsDelete();
        relativeLayout = null;
    }


    //ActivityまたはViewのタップイベントを受け取り、処理すべき操作が発生したかどうかを判断して、実行に移す（今回はタップした座標を入手するだけなので、特に指定せず）
    public boolean onTouchEvent(MotionEvent event) {
    //繰り返し処理（動き＋当たり判定）
        runnable = new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void run() {
                //ゲーム進行
                if (gameGamenFlag) {
                    //ゲームスタート画面フラグはOFF
                    gameStartGamenFlag = false;

                    //スタート、リスタート非表示
                    textView[0].setText("");
                    textView[7].setText("");

                    //ボールサーバーを透明にする
                    server.setAlpha(0.85f);

                    //指のX,Y座標を取得
                    tapx = (int) event.getX();       //受け取った（引数の）MotionEvent内のフィールドからx座標を入手

                    //バーフラグを設定
                    countBar++;
                    if (countBar >= 0) {
                        barFlag = true;
                    }

                    //スモークを設定
                    if (smoke.top > height) {
                        //初期位置に戻す
                        smoke.bottom = 0;
                        smoke.isVisible = false;
                        smoke.setVisibility(View.INVISIBLE);
                    }

                    //動き
                    //指のX座標をもとにbarを動かす
                    bar.move(barFlag, tapx, width);

                    //ボールを動かす
                    for (Ball ball:balls) {
                        if (keepBalls.contains(ball)) {
                        }
                        else    ball.move();
                    }

                    //反射ブロックを動かす
                    for (int num = 0; num < moveBlockNum; num++) {
                        moveBlock[num].move();
                    }

                    //アイテムを動かす(落下)
                    for (int r = 0; r < rowSize; r++) {
                        for (int c = 0; c < colSize; c++) {
                            if (item[c][r].isVisible) {
                                item[c][r].move();
                            }
                        }
                    }
                    if (smoke.isVisible) {
                        smoke.setVisibility(View.VISIBLE);
                        smoke.move();
                    }

                    //クラゲを動かす
                    for (int num = 0; num < kurageNum; num++) {
                        kurage[num].move();
                    }

                    //当たり判定用変数を更新
                    for (Ball ball:balls) {
                        if (ball.dx > 0) {
                            serverRangeX = ball.dx / 2;
                            blockRangeX = ball.dx / 2;
                        }
                        else {
                            serverRangeX = -ball.dx / 2;
                            blockRangeX = -ball.dx / 2;
                        }
                        if (ball.dy > 0) {
                            barRangeY = ball.dy / 2;
                            serverRangeY = ball.dy / 2;
                            blockRangeY = ball.dy / 2;
                            moveBlockRangeY = ball.dy / 2;
                        }
                        else {
                            barRangeY = -ball.dy / 2;
                            serverRangeY = -ball.dy / 2;
                            blockRangeY = -ball.dy / 2;
                            moveBlockRangeY = -ball.dy / 2;
                        }
                    }

                    //当たり判定処理
                    hitProcess();

                    //ボールが全て画面下端に出たら、ゲームオーバー
                    if (ballNum == 0 && gameGamenFlag){
                        gameProcess('o');
                    }
                    //ブロックを全て壊したらゲームクリア
                    if (breakBlocks == rowSize * colSize && gameGamenFlag) {
                        gameProcess('c');
                    }

                    handler.removeCallbacks(runnable);                  // 10ミリ秒ごとに繰り返すと、handlerがダブってしまうので前の処理を削除
                    //バー
                    relativeLayout.removeView(bar);                     // 次の位置に描きたいので、前のViewを削除（Viewもダブってしまうため）
                    relativeLayout.addView(bar);                        // 次の位置に描く（再び描く処理）
                    //スモーク
                    relativeLayout.removeView(smoke);
                    relativeLayout.addView(smoke);
                    //ボール
                    for (Ball ball:balls) {
                        relativeLayout.removeView(ball);
                        relativeLayout.addView(ball);
                    }
                    //ボールサーバー（ボールの上に描かれるようにするため追加）
                    relativeLayout.removeView(server);
                    relativeLayout.addView(server);
                    //動く反射ブロック
                    for (int num = 0; num < moveBlockNum; num++) {
                        relativeLayout.removeView(moveBlock[num]);
                        relativeLayout.addView(moveBlock[num]);
                    }
                    //アイテム
                    for (int r = 0; r < rowSize; r++) {
                        for (int c = 0; c < colSize; c++) {
                            if (item[c][r].isVisible) {
                                relativeLayout.removeView(item[c][r]);
                                relativeLayout.addView(item[c][r]);
                            }
                        }
                    }
                    //クラゲ
                    for (int num = 0; num < kurageNum; num++) {
                        relativeLayout.removeView(kurage[num]);
                        relativeLayout.addView(kurage[num]);
                    }
                }
                handler.postDelayed(runnable, 10);          // 10ミリ秒ごとに繰り返し（次回処理をセット）
            }
        };
        handler.postDelayed(runnable, 10);              // 10ミリ秒ごとに繰り返し（初回実行）
        return super.onTouchEvent(event);
    }


    //当たり判定（絵に描くとイメージしやすい）
    @SuppressLint("Range")
    private void hitProcess(){
        //バー
        //当たり判定メソッドを呼び出す
        ballHit(bar.left, bar.top, bar.right, bar.bottom, 0, 0, false);

        //ブロック
        for (int row = 0; row < rowSize; row++) {
            for (int col = 0; col < colSize; col++) {

                //当たり判定メソッドを呼び出す
                ballHit(block[col][row].left, block[col][row].top, block[col][row].right, block[col][row].bottom, 0, 0, block[col][row].breakable);

                //ボールに当たった場合にブロックを壊す
                if(isBreak) {
                    breakBlock(col, row);
                    //フラグを元に戻す
                    isBreak = false;
                }
            }
        }

        //動く反射ブロック
        for (int num = 0; num < moveBlockNum; num++) {
            //当たり判定メソッドを呼び出す
            ballHit(moveBlock[num].left, moveBlock[num].top, moveBlock[num].right, moveBlock[num].bottom, moveBlock[num].dx, 0, false);
        }

        for (Ball ball:balls) {
            //画面
            //画面左面
            if (ball.x <= ball.radius && ball.dx < 0) {
                ball.dx = -ball.dx;
                //画面右面
            } else if (ball.x >= width - ball.radius && ball.dx > 0) {
                ball.dx = -ball.dx;
                //画面上面
            } else if (ball.y <= ball.radius && ball.dy < 0) {
                ball.dy = -ball.dy;
                //画面下面
            } else if (ball.y >= height) {
                //画面上のボール数を-1
                ballNum--;
                //ボールをボールサーバー内に戻す
                keepBalls.add(ball);
                //ボールを初期配置に戻す
                r3 = new Random();
                int random3 = r3.nextInt(5);
                ball.x = server.left + (server.right - server.left) / 3 + (server.right - server.left) / 12 * random3;
                ball.y = (server.top + server.bottom) / 2;
            }

            //ワープの当たり判定
            //（四角形の四辺に対してボールが重なっていたら）
            for (int num = 0; num < warpNum; num++) {
                if ((ball.x > warp[num].left) &&
                        (ball.x < warp[num].right) &&
                        (ball.y > warp[num].top) &&
                        (ball.y < warp[num].bottom)) {
                    r1 = new Random();
                    int random1 = r1.nextInt(warpNum);
                    if (random1 == num){
                        for (;;){
                            r = new Random();
                            int random = r.nextInt(warpNum);
                            random1 = random;
                            if (random1 != num)     break;
                        }
                    }
                    r2 = new Random();
                    int random2 = r2.nextInt(4);
                    switch (random2){
                        case 0: {   //ワープの左上から出てくる
                            ball.x = warp[random1].left;
                            ball.y = warp[random1].top;
                            if (ball.dx > 0){
                                ball.dx = -ball.dx;
                            }
                            if (ball.dy > 0){
                                ball.dy = -ball.dy;
                            }
                            break;
                        }
                        case 1: {   //ワープの左下から出てくる
                            ball.x = warp[random1].left;
                            ball.y = warp[random1].bottom;
                            if (ball.dx > 0){
                                ball.dx = -ball.dx;
                            }
                            if (ball.dy < 0){
                                ball.dy = -ball.dy;
                            }
                            break;
                        }
                        case 2: {   //ワープの右下から出てくる
                            ball.x = warp[random1].right;
                            ball.y = warp[random1].bottom;
                            if (ball.dx < 0){
                                ball.dx = -ball.dx;
                            }
                            if (ball.dy < 0){
                                ball.dy = -ball.dy;
                            }
                            break;
                        }
                        case 3: {   //ワープの右上から出てくる
                            ball.x = warp[random1].right;
                            ball.y = warp[random1].top;
                            if (ball.dx < 0){
                                ball.dx = -ball.dx;
                            }
                            if (ball.dy > 0){
                                ball.dy = -ball.dy;
                            }
                            break;
                        }
                    }
                }
            }

            //ムービングの当たり判定
            //（四角形の四辺に対してボールが重なっていたら）
            for (int num = 0; num < movingNum; num++) {
                if ((ball.x + ball.radius > moving[num].left) &&           //判定を厳しくする → ボールがムービングに半分食い込んだら判定
                        (ball.x - ball.radius < moving[num].right) &&
                        (ball.y > moving[num].top + (moving[num].bottom - moving[num].top) / 3 &&
                                (ball.y < moving[num].bottom - (moving[num].bottom - moving[num].top) / 3))) {
                    if (ball.dy != 0){
                        r = new Random();
                        int random = r.nextInt(2);
                        if (random == 0){
                            ball.dx = -ball.dx;             //1/2の確率で水平方向反転する
                        }
                    }
                    ball.dy = 0;
                }
                if(ball.dy == 0){
                    //画面左面
                    if (ball.x <= ball.radius) {
                        ball.dy = height / 228;           //ball.dy = 10;
                    }
                    //画面右面
                    else if (ball.x >= width - ball.radius) {
                        ball.dy = height / 228;           //ball.dy = 10;
                    }
                }
            }

            //クラゲの当たり判定
            //（四角形の四辺に対してバーが重なっていたら）
            for (int num = 0; num < kurageNum; num++) {
                if (kurage[num].isVisible) {
                    if ((kurage[num].right > bar.left) &&
                            (kurage[num].left < bar.right) &&
                            (kurage[num].bottom > bar.top) &&
                            (kurage[num].top < bar.bottom)) {
                      //一定時間、バー不動
                        countBar = -50;           //countがマイナスになり、count>0になるまでバー不動
                        barFlag = false;
                    }
                }
            }

            //アイテムの当たり判定
            //（四角形の四辺に対してバーが重なっていたら）
            for (int r = 0; r < rowSize; r++) {
                for (int c = 0; c < colSize; c++) {
                    if (item[c][r].isVisible) {
                        if ((item[c][r].right > bar.left) &&
                                (item[c][r].left < bar.right) &&
                                (item[c][r].bottom > bar.top) &&
                                (item[c][r].top < bar.bottom)) {
                            item[c][r].setVisibility(View.INVISIBLE);
                            item[c][r].isVisible = false;
                            switch (item[c][r].itemRandom) {
                                case 0://バーdownサイズ
                                    bar.size += 1;
                                    if (bar.size >= 4)  bar.size = 4;      //最小値
                                    break;
                                case 1://バーupサイズ
                                    bar.size -= 1;
                                    if (bar.size <= -4)  bar.size = -4;      //最大値
                                    break;
                                case 2://ボール速度down
                                    if (ball.dx > 7 || ball.dx < -7) {
                                        ball.dx *= 0.9;
                                        ball.dy *= 0.9;
                                    }
                                    break;
                                case 3://ボール速度up
                                    if (ball.dx < 13 && ball.dx > -13) {
                                        ball.dx *= 1.1;
                                        ball.dy *= 1.1;
                                    }
                                    break;
                                case 4://ボール増加
                                    //画面へ放出するボールの数を決める
                                    ballCount = keepBalls.size();
                                    //ボールサーバー内から取り出す（ボールを放出）
                                    for (int i = 0; i < ballCount; i++) {
                                        keepBalls.remove(0);
                                        ballNum++;
                                    }
                                    break;
                            }
                        }
                        if (item[c][r].top > height) {              //画面下外に出たら動きを止め、ビューを削除
                            item[c][r].dy = 0;
                            //アイテムを画面外に
                            item[c][r].left = -height / 46;             //item[c][r].left = -50;
                            item[c][r].top = -height / 46;              //item[c][r].top = -50;
                            item[c][r].setVisibility(View.GONE);
                            item[c][r].isVisible = false;
                        }
                    }
                }
            }
        }
    }


    //当たり判定実装メソッド
    public void ballHit(int left, int top, int right, int bottom, int dx, int dy, boolean isBreakObject) {
        for (Ball ball:balls) {
            //ボールの相対速度x(ボールのx速度ー動くオブジェクトのx速度)から、判定の境界線範囲を決める
            int rangeX = (ball.dx - dx) / 2;
            if (rangeX < 0){
                rangeX = -rangeX;
            }
            int rangeY = (ball.dy - dy) / 2;
            if (rangeY < 0){
                rangeY = -rangeY;
            }

            //左面
            if ((ball.x + ball.radius <= left + rangeX) &&
                    (ball.x + ball.radius >= left - rangeX) &&
                    (ball.y <= bottom + rangeY) &&
                    (ball.y >= top - rangeY) &&
                    (ball.dx > 0)) {
                ball.dx = -ball.dx;
                if (isBreakObject) {
                    isBreak = true;
                }
            }
            //右面
            if ((ball.x - ball.radius <= right + rangeX) &&
                    (ball.x - ball.radius >= right - rangeX) &&
                    (ball.y <= bottom + rangeY) &&
                    (ball.y >= top - rangeY) &&
                    (ball.dx < 0)) {
                ball.dx = -ball.dx;
                if (isBreakObject) {
                    isBreak = true;
                }
            }
            //下面
            if ((ball.y - ball.radius <= bottom + rangeY) &&
                    (ball.y - ball.radius >= bottom - rangeY) &&
                    (ball.x >= left - rangeX) &&
                    (ball.x <= right + rangeX) &&
                    (ball.dy < 0)) {
                ball.dy = -ball.dy;
                if (isBreakObject) {
                    isBreak = true;
                }
            }
            //上面
            if ((ball.y + ball.radius <= top + rangeY) &&
                    (ball.y + ball.radius >= top - rangeY) &&
                    (ball.x >= left - rangeX) &&
                    (ball.x <= right + rangeX) &&
                    (ball.dy > 0)) {
                ball.dy = -ball.dy;
                if (isBreakObject) {
                    isBreak = true;
                }
            }
            //角にボールが当たった時の当たり判定
            //Math.pow：2乗
            double leftSpace = Math.pow(left - ball.x, 2);
            double bottomSpace = Math.pow(bottom - ball.y, 2);
            double rightSpace = Math.pow(right - ball.x, 2);
            double topSpace = Math.pow(top - ball.y, 2);
            //左下角
            if (leftSpace + bottomSpace <= Math.pow(ball.radius, 2)) {
                //ボールが右下斜め方向に進んでいた時
                if (ball.dx > 0 && ball.dy > 0) {
                    ball.dx = -ball.dx;
                }
                //ボールが右上斜め方向に進んでいた時
                if (ball.dx > 0 && ball.dy < 0) {
                    ball.dx = -ball.dx;
                    ball.dy = -ball.dy;
                }
                //ボールが左上斜め方向に進んでいた時
                if (ball.dx < 0 && ball.dy < 0) {
                    ball.dy = -ball.dy;
                }
                if (isBreakObject) {
                    isBreak = true;
                }
            }
            //左上角
            if (leftSpace + topSpace <= Math.pow(ball.radius, 2)) {
                //ボールが右下斜め方向に進んでいた時
                if (ball.dx > 0 && ball.dy > 0) {
                    ball.dx = -ball.dx;
                    ball.dy = -ball.dy;
                }
                //ボールが右上斜め方向に進んでいた時
                if (ball.dx > 0 && ball.dy < 0) {
                    ball.dx = -ball.dx;
                }
                //ボールが左下斜め方向に進んでいた時
                if (ball.dx < 0 && ball.dy > 0) {
                    ball.dy = -ball.dy;
                }
                if (isBreakObject) {
                    isBreak = true;
                }
            }
            //右下角
            if (rightSpace + bottomSpace <= Math.pow(ball.radius, 2)) {
                //ボールが左上斜め方向に進んでいた時
                if (ball.dx < 0 && ball.dy < 0) {
                    ball.dx = -ball.dx;
                    ball.dy = -ball.dy;
                }
                //ボールが右上斜め方向に進んでいた時
                if (ball.dx > 0 && ball.dy < 0) {
                    ball.dy = -ball.dy;
                }
                //ボールが左下斜め方向に進んでいた時
                if (ball.dx < 0 && ball.dy > 0) {
                    ball.dx = -ball.dx;
                }
                if (isBreakObject) {
                    isBreak = true;
                }
            }
            //右上角
            if (rightSpace + topSpace <= Math.pow(ball.radius, 2)) {
                //ボールが左上斜め方向に進んでいた時
                if (ball.dx < 0 && ball.dy < 0) {
                    ball.dx = -ball.dx;
                }
                //ボールが右下斜め方向に進んでいた時
                if (ball.dx > 0 && ball.dy > 0) {
                    ball.dy = -ball.dy;
                }
                //ボールが左下斜め方向に進んでいた時
                if (ball.dx < 0 && ball.dy > 0) {
                    ball.dx = -ball.dx;
                    ball.dy = -ball.dy;
                }
                if (isBreakObject) {
                    isBreak = true;
                }
            }
        }
    }


    //ブロックを壊す処理
    public void breakBlock(int col, int row) {
        block[col][row].top = -height / 46;             //block[col][row].top = -50;
        block[col][row].bottom = -height / 46;          //block[col][row].bottom = -50;
        block[col][row].setVisibility(View.GONE);   //GONE：非表示（空白部分を詰める）　//INVISIBLE：非表示（配置そのまま）
        breakBlocks++;                              //壊したブロックの数＋1

        //ドロップアイテム出現
        r = new Random();
        int Random = r.nextInt(item[col][row].dropRate);        //ドロップアイテム出現確率
        if (Random == 1){                                           //ブロックが壊れた際、ランダムでドロップアイテム出現
            item[col][row].isVisible = true;
            item[col][row].setVisibility(View.VISIBLE);
        }
    }

    //アニメーション
    public void objectsAppear(int status){
        switch (status){
            case 0: {       //画面OUT
                //ボール
                for (Ball ball:balls) {
                    startAnimation(ball, height, 0);
                }
                //ブロック
                for (int row = 0; row < rowSize; row++) {
                    for (int col = 0; col < colSize; col++) {
                        startAnimation(block[col][row], height, 0);
                    }
                }
                //動く反射ブロック
                for (int num = 0; num < moveBlockNum; num++) {
                    startAnimation(moveBlock[num], height, 0);
                }
                //アイテム
                for (int r = 0; r < rowSize; r++) {
                    for (int c = 0; c < colSize; c++) {
                        if (item[c][r].isVisible) {
                            startAnimation(item[c][r], height, 0);
                        }
                    }
                }
                //ワープ
                for (int num = 0; num < warpNum; num++) {
                    startAnimation(warp[num], height, 0);
                }
                //ムービング
                for (int num = 0; num < movingNum; num++) {
                    startAnimation(moving[num], height, 0);
                }
                //ボールサーバー
                startAnimation(server, height, 0);
            }
            break;
            case 1: {       //画面IN
                //ボール
                for (Ball ball:balls) {
                    startAnimation(ball, height, 1);
                }
                //ブロック
                for (int row = 0; row < rowSize; row++) {
                    for (int col = 0; col < colSize; col++) {
                        startAnimation(block[col][row], height, 1);
                    }
                }
                //動く反射ブロック
                for (int num = 0; num < moveBlockNum; num++) {
                    startAnimation(moveBlock[num], height, 1);
                }
                //アイテム
                for (int r = 0; r < rowSize; r++) {
                    for (int c = 0; c < colSize; c++) {
                        if (item[c][r].isVisible) {
                            startAnimation(item[c][r], height, 1);
                        }
                    }
                }
                //ワープ
                for (int num = 0; num < warpNum; num++) {
                    startAnimation(warp[num], height, 1);
                }
                //ムービング
                for (int num = 0; num < movingNum; num++) {
                    startAnimation(moving[num], height, 1);
                }
                //ボールサーバー
                startAnimation(server, height, 1);
            }
            break;
        }
    }

    //アニメーション実装メソッド
    private void startAnimation(View view, int height, int type) {
        switch (type) {
            case 0: {       //画面OUT
                translateAnimation = new TranslateAnimation(
                        Animation.ABSOLUTE, 0.0f,
                        Animation.ABSOLUTE, 0.0f,
                        Animation.ABSOLUTE, 0.0f,
                        Animation.ABSOLUTE, height * 2);
                translateAnimation.setDuration(4000);
                translateAnimation.setRepeatCount(0);
                translateAnimation.setFillAfter(true);
                view.startAnimation(translateAnimation);
            }
            break;
            case 1: {       //画面IN
                translateAnimation = new TranslateAnimation(
                        Animation.ABSOLUTE, 0.0f,
                        Animation.ABSOLUTE, 0.0f,
                        Animation.ABSOLUTE, -height,
                        Animation.ABSOLUTE, 0.0f);
                translateAnimation.setDuration(4000);
                translateAnimation.setRepeatCount(0);
                translateAnimation.setFillAfter(true);
                view.startAnimation(translateAnimation);
            }
            break;
            case 2: {       //テキストフェードアウト
                alphaAnimation = new AlphaAnimation(1.0f, 0.0f);        //透明度を1から0に変化
                alphaAnimation.setRepeatCount(0);
                alphaAnimation.setDuration(700);
                alphaAnimation.setFillAfter(true);
                view.startAnimation(alphaAnimation);
            }
            break;
            case 3: {       //テキスト強調
                alphaAnimation = new AlphaAnimation(0.3f, 0.8f);        //透明度を0.4から0.8に変化
                alphaAnimation.setRepeatCount(-1);                      //繰り返し回数＝リピート
                alphaAnimation.setDuration(700);
                alphaAnimation.setRepeatMode(2);                        //繰り返しモード：reverse
                alphaAnimation.setFillAfter(true);
                view.startAnimation(alphaAnimation);
            }
            break;
        }
    }

}