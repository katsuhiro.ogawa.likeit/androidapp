package com.example.myapp_block_2;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.view.View;


public class LightRing extends View {
    int left, top, right, bottom;
    Paint paint1, paint2;
    Path path1, path2;

    //コンストラクタ生成
    public LightRing(Context context) {
        super(context);
        left = 0;
        top = 0;
        right = 0;
        bottom = 0;
        //ペイント1
        paint1 = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint1.setStyle(Paint.Style.FILL_AND_STROKE);
        paint1.setColor(Color.WHITE);
        paint1.setPathEffect(new CornerPathEffect(50));
        paint1.setAlpha(180);        //半透明描写(0で完全透明、255で無透明)
        //ペイント2(内側)
        paint2 = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint2.setStyle(Paint.Style.FILL_AND_STROKE);
        paint2.setColor(Color.parseColor("#20b2aa"));
        paint2.setPathEffect(new CornerPathEffect(50));
    }

    //onDraw()メソッド
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        //パス1
        path1 = new Path();
        path1.rewind();
        path1.moveTo(left + (right - left) / 4, top);
        path1.lineTo(right - (right - left) / 4, top);
        path1.lineTo(right, top + (bottom - top) / 4);
        path1.lineTo(right, bottom - (bottom - top) / 4);
        path1.lineTo(right - (right - left) / 4, bottom);
        path1.lineTo(left + (right - left) / 4, bottom);
        path1.lineTo(left, bottom - (bottom - top) / 4);
        path1.lineTo(left, top + (bottom - top) / 4);
        path1.close();
        //パスで描く
        canvas.drawPath(path1, paint1);

        //パス2(内側)
        path2 = new Path();
        path2.rewind();
        path2.moveTo(left + (right - left) * 3 / 8, top + (bottom - top) / 3);
        path2.lineTo(right - (right - left) * 3 / 8, top + (bottom - top) / 3);
        path2.lineTo(right - (right - left) / 8, top + (bottom - top) * 3 / 7);
        path2.lineTo(right - (right - left) / 8, bottom - (bottom - top) * 3 / 7);
        path2.lineTo(right - (right - left) * 3 / 8, bottom - (bottom - top) / 3);
        path2.lineTo(left + (right - left) * 3 / 8, bottom - (bottom - top) / 3);
        path2.lineTo(left + (right - left) / 8, bottom - (bottom - top) * 3 / 7);
        path2.lineTo(left + (right - left) / 8, top + (bottom - top) * 3 / 7);
        path2.close();
        //パスで描く
        canvas.drawPath(path2, paint2);
    }
}
