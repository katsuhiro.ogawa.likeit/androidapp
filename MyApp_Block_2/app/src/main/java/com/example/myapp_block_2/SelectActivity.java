package com.example.myapp_block_2;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;


public class SelectActivity extends AppCompatActivity {
    Button bt1Click, bt2Click, bt3Click, bt4Click, bt5Click, bt6Click;
    TextView textView1, textView2, textView3, textView4, textView5, textView6;
    ImageButton  bt7Click, bt8Click, bt9Click;
    int selectGamen = 0, stage = 0, page = 2;
    List<Button> buttons;
    List<TextView> textViews;
    SharedPreferences sharedPref;       //共有プリファレンスの変数を用意(クリアデータが格納されている)

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select);

        //ナビゲーションバーとステータスバーを非表示にする
        View decor = getWindow().getDecorView();
        // hide navigation bar, show status bar
        decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        // show navigation bar, hide status bar
        decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);

        //ボタンを用意
        bt1Click = findViewById(R.id.sl1Btn1);
        bt2Click = findViewById(R.id.sl1Btn2);
        bt3Click = findViewById(R.id.sl1Btn3);
        bt4Click = findViewById(R.id.sl1Btn4);
        bt5Click = findViewById(R.id.sl1Btn5);
        bt6Click = findViewById(R.id.sl1Btn6);
        bt7Click = findViewById(R.id.homeBtn);
        bt8Click = findViewById(R.id.backBtn);
        bt9Click = findViewById(R.id.nextBtn);
        //ボタンを配列に格納
        buttons = new ArrayList<Button>();
        buttons.add(bt1Click);
        buttons.add(bt2Click);
        buttons.add(bt3Click);
        buttons.add(bt4Click);
        buttons.add(bt5Click);
        buttons.add(bt6Click);
        //フォントを取得し、ボタンに設定（フォントデータはassetsファイルに入っている）
        Typeface customFont1 = Typeface.createFromAsset(getAssets(), "LibreBaskerville-Regular.ttf");
        for (Button button : buttons) {
            button.setTypeface(customFont1);
        }
        //ボタンにリスナーを設定
        StageListener listener = new StageListener();
        for (Button button : buttons) {
            button.setOnClickListener(listener);
        }
        bt7Click.setOnClickListener(listener);
        bt8Click.setOnClickListener(listener);
        bt9Click.setOnClickListener(listener);

        //テキスト(clear)を用意
        textView1 = findViewById(R.id.clear1);
        textView2 = findViewById(R.id.clear2);
        textView3 = findViewById(R.id.clear3);
        textView4 = findViewById(R.id.clear4);
        textView5 = findViewById(R.id.clear5);
        textView6 = findViewById(R.id.clear6);
        //テキストを配列に格納
        textViews = new ArrayList<TextView>();
        textViews.add(textView1);
        textViews.add(textView2);
        textViews.add(textView3);
        textViews.add(textView4);
        textViews.add(textView5);
        textViews.add(textView6);

        //共有プリファレンスを用意(クリアテキスト表示)
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        //テキストの表示切り替え
        int i = 1;
        for (TextView textView : textViews) {
            stage = selectGamen * 6 + i;
            int data = sharedPref.getInt("" + stage, 0);    //共有プリファレンスからデータを取り出す(0:未達成 1:達成)
            if (data == 1) {
                textView.setVisibility(View.VISIBLE);       // クリア表示
            } else textView.setVisibility(View.INVISIBLE);
            i++;
        }
    }

    private class StageListener implements View.OnClickListener{
        @Override
        public void onClick(View view){
            int id = view.getId();
            switch(id){
                case R.id.sl1Btn1: {
                    Intent intent = new Intent(SelectActivity.this, MainActivity.class);
                    stage = selectGamen * 6 + 1;
                    intent.putExtra("stageNum", stage);
                    startActivity(intent);
                    break;
                }

                case R.id.sl1Btn2: {
                    Intent intent = new Intent(SelectActivity.this, MainActivity.class);
                    stage = selectGamen * 6 + 2;
                    intent.putExtra("stageNum", stage);
                    startActivity(intent);
                    break;
                }

                case R.id.sl1Btn3: {
                    Intent intent = new Intent(SelectActivity.this, MainActivity.class);
                    stage = selectGamen * 6 + 3;
                    intent.putExtra("stageNum", stage);
                    startActivity(intent);
                    break;
                }

                case R.id.sl1Btn4: {
                    Intent intent = new Intent(SelectActivity.this, MainActivity.class);
                    stage = selectGamen * 6 + 4;
                    intent.putExtra("stageNum", stage);
                    startActivity(intent);
                    break;
                }

                case R.id.sl1Btn5: {
                    Intent intent = new Intent(SelectActivity.this, MainActivity.class);
                    stage = selectGamen * 6 + 5;
                    intent.putExtra("stageNum", stage);
                    startActivity(intent);
                    break;
                }

                case R.id.sl1Btn6: {
                    Intent intent = new Intent(SelectActivity.this, MainActivity.class);
                    stage = selectGamen * 6 + 6;
                    intent.putExtra("stageNum", stage);
                    startActivity(intent);
                    break;
                }

                case R.id.homeBtn: {
                    Intent intent = new Intent(SelectActivity.this, StartActivity.class);
                    startActivity(intent);
                    break;
                }

                case R.id.backBtn: {
                    if (selectGamen > 0) {
                        selectGamen--;
                        changeView();
                    }
                    break;
                }

                case R.id.nextBtn: {
                    if (selectGamen < page - 1) {
                        selectGamen++;
                        changeView();
                    }
                    break;
                }
            }
        }
    }

    //画面の表示を変える(select画面切り替え)
    public void changeView() {
        int i = 1;
        //ボタンの表示切り替え
        for (Button button:buttons) {
            stage = selectGamen * 6 + i;
            button.setText("STAGE " + stage);
            i++;
        }
        i = 1;
        //テキストの表示切り替え
        for (TextView textView:textViews) {
            stage = selectGamen * 6 + i;
            int data = sharedPref.getInt("" + stage, 0);    //共有プリファレンスからデータを取り出す(0:未達成 1:達成)
            if (data == 1) {
                textView.setVisibility(View.VISIBLE);       // クリア表示
            } else textView.setVisibility(View.INVISIBLE);
            i++;
        }
    }
}