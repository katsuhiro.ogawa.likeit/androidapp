package com.example.myapp_block_2;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Shader;
import android.view.View;


public class Smoke extends View {
    int left, top, right, bottom, dy;
    boolean isVisible;
    Paint paint;
    Rect rect;
    LinearGradient lig;

    //コンストラクタ生成
    public Smoke(Context context) {
        super(context);
        left = 0;
        top = 0;
        right = 0;
        bottom = 0;
        dy = 0;     //落下速度
        isVisible = false;
        this.setVisibility(INVISIBLE);
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setAlpha(200);        //半透明描写(0で完全透明、255で無透明)
    }

    //onDraw()メソッド
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        //グラデーション
        int[] colors = { 0xFFffffff, 0xFFffffdd, 0xFFffffff};
        lig = new LinearGradient(left, (top + bottom) / 2, right, (top + bottom) / 2, colors, null, Shader.TileMode.CLAMP);
        paint.setShader(lig);

        //四角形を描く
        rect = new Rect(left, top, right, bottom);
        canvas.drawRect(rect, paint);
    }

    //move()メソッド
    protected void move() {
        top += dy;
        bottom += dy;
    }
}
