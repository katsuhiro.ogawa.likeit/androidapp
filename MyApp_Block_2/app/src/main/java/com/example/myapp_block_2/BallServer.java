package com.example.myapp_block_2;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.view.View;


public class BallServer extends View {
    int width, height;
    int left, top, right, bottom;
    Paint paint;
    RadialGradient rg;
    Path path;

    //コンストラクタ生成
    public BallServer(Context context) {
        super(context);
        width = 0;
        height = 0;
        left = 0;
        top = 0;
        right = 0;
        bottom = 0;
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Paint.Style.FILL);
        paint.setPathEffect(new CornerPathEffect(50));
        paint.setAlpha(200);        //半透明描写(0で完全透明、255で無透明)
    }

    //onDraw()メソッド
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //パス
        path = new Path();
        path.rewind();
        path.moveTo(left + (right - left) / 4, top);
        path.lineTo(right - (right - left) / 4, top);
        path.lineTo(right, top + (bottom - top) / 4);
        path.lineTo(right - (right - left) / 4, top + (bottom - top) / 2);
        path.lineTo(right, bottom - (bottom - top) / 4);
        path.lineTo(right - (right - left) / 4, bottom);
        path.lineTo(left + (right - left) / 4, bottom);
        path.lineTo(left, bottom - (bottom - top) / 4);
        path.lineTo(left + (right - left) / 4, top + (bottom - top) / 2);
        path.lineTo(left, top + (bottom - top) / 4);
        path.close();

        //グラデーション
        int[] colors = { 0xFFffffff, 0xFFbdb76b, 0xFF777777};
        rg = new RadialGradient((left + right) / 2, (top + bottom) / 2, (bottom - top) / 2, colors, null, Shader.TileMode.CLAMP);
        paint.setShader(rg);

        //パスで描く
        canvas.drawPath(path, paint);
    }
}
