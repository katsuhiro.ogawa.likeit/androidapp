package com.example.myapp_block_2;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.CornerPathEffect;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.view.View;
import java.util.Random;


public class Item extends View {
    int left, top, right, bottom, itemRandom, dropRate, dy;
    static int itemNum;
    Paint paint;
    RectF rectf;
    boolean isVisible;
    Random r;
    LinearGradient lig;

    //コンストラクタ生成
    public Item(Context context) {      //Blockのインスタンスを生成する際にActivityの情報を渡す
        super(context);
        isVisible = false;
        itemNum = 5;            //アイテム種類
        dropRate = 3;           //アイテムの出現確率（左の回数の内、１回出現 = 1/dropRate）
        dy = 0;                //アイテムの落下速度
        left = 0;
        top = 0;
        right = 0;
        bottom = 0;
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setPathEffect(new CornerPathEffect(50));
    }

    //onDraw()メソッド
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);                           //画面にCanvasを描写

        //グラデーション
        switch (itemRandom){
            //サイズdown（暗青）
            case 0:
//                int[] colors0 = { 0xFF003399, 0xFF0066cc, 0xFF003399 };
                int[] colors0 = { 0xFF000033, 0xFF0033ff, 0xFF000033 };
                lig = new LinearGradient(left, top, right, bottom, colors0, null, Shader.TileMode.CLAMP);
                paint.setShader(lig);
                break;
            //サイズup（青）
            case 1:
//                int[] colors1 = { 0xFF00cccc, 0xFF00ffcc, 0xFF00cccc };
//                int[] colors1 = { 0xFF00ffcc, 0xFFccffff, 0xFF00ffcc };
//                int[] colors1 = { 0xFF0066ff, 0xFF0099cc, 0xFF0066ff };
                int[] colors1 = { 0xFF0033ff, 0xFF0099ff, 0xFF0033ff };
                lig = new LinearGradient(left, top, right, bottom, colors1, null, Shader.TileMode.CLAMP);
                paint.setShader(lig);
                break;
            //速度down（暗赤）
            case 2:
//                int[] colors2 = { 0xFF993366, 0xFFcc3366, 0xFF993366 };
//                int[] colors2 = { 0xFFcc3366, 0xFFcc6600, 0xFFcc3366 };
//                int[] colors2 = { 0xFF990000, 0xFFcc0000, 0xFF990000 };
                int[] colors2 = { 0xFF660000, 0xFFcc0000, 0xFF660000 };
                lig = new LinearGradient(left, top, right, bottom, colors2, null, Shader.TileMode.CLAMP);
                paint.setShader(lig);
                break;
            //速度up（赤）
            case 3:
//                int[] colors3 = { 0xFFcc9900, 0xFFffcc66, 0xFFcc9900 };
//                int[] colors3 = { 0xFFcc66cc, 0xFFff99cc, 0xFFcc66cc };
//                int[] colors3 = { 0xFFcc3366, 0xFFcc6600, 0xFFcc3366 };
                int[] colors3 = { 0xFFcc3399, 0xFFcc6600, 0xFFcc3399 };
                lig = new LinearGradient(left, top, right, bottom, colors3, null, Shader.TileMode.CLAMP);
                paint.setShader(lig);
                break;
            //ボール増加（金色）
            case 4:
//                int[] colors4 = { 0xFFeee8aa, 0xFFfff8dc, 0xFFeee8aa };
                int[] colors4 = { 0xFFeee8aa, 0xFFffd700, 0xFFeee8aa };
                lig = new LinearGradient(left, top, right, bottom, colors4, null, Shader.TileMode.CLAMP);
                paint.setShader(lig);
                break;
        }

        //円を描く
        rectf = new RectF(left, top, right, bottom);
        canvas.drawOval(rectf, paint);
    }

    //move()メソッド
    protected void move() {
        top += dy;
        bottom += dy;
    }
}