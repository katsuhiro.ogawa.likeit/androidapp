package com.example.myapp_block_2;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.view.View;


public class Moving extends View {
    int left, top, right, bottom;
    Paint paint;
    RectF rectf;
    LinearGradient lig;

    //コンストラクタ生成
    public Moving(Context context) {
        super(context);
        left = 0;
        top = 0;
        right = 0;
        bottom = 0;
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setAlpha(100);        //半透明描写(0で完全透明、255で無透明)
    }

    //onDraw()メソッド
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        //グラデーション
        int[] colors = { 0xFF000000, 0xFFffffff, 0xFF000000};
        lig = new LinearGradient(left, top, right, bottom, colors, null, Shader.TileMode.CLAMP);
        paint.setShader(lig);
        rectf = new RectF(left, top, right, bottom);

        //円を描く
        canvas.drawOval(rectf, paint);
    }
}

