package com.example.myapp_block_2;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RadialGradient;
import android.graphics.RectF;
import android.graphics.Shader;
import android.view.View;


public class Block extends View {       //描画の際はViewクラスを継承
    int left, top, right, bottom;
    boolean breakable;                  //breakableフラグ：壊れるブロックかどうか
    int type;
    Paint paint;                        //BlockクラスのフィールドにPaintクラスのインスタンスを用意（イメージ：ぺんを持っている）
    RectF rectf;
    RadialGradient rg;
    Path path;

    //コンストラクタ生成
    public Block(Context context) {      //Blockのインスタンスを生成する際にActivityの情報を渡す
        super(context);
        left = 0;       //ブロックの初期位置
        top = 0;
        right = 0;
        bottom = 0;
        breakable = true;
        type = 0;
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    }

    //onDraw()メソッド
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //パス（貝殻）
        path = new Path();
        path.rewind();
        path.moveTo(left, bottom - (bottom - top) / 3);
        path.lineTo(left, top + (bottom - top) / 3);
        path.lineTo(left + (right - left) / 5, top);
        path.lineTo(right - (right - left) / 5, top);
        path.lineTo(right, top + (bottom - top) / 3);
        path.lineTo(right, bottom - (bottom - top) / 3);
        path.lineTo(right - (right - left) / 3, bottom - (bottom - top) / 9);
        path.lineTo(right - (right - left) / 7, bottom);
        path.lineTo(left + (right - left) / 7, bottom);
        path.lineTo(left + (right - left) / 3, bottom - (bottom - top) / 9);
        path.close();

        switch (type) {
            case 1: {
              //グラデーション
                int[] colors = { 0xFFd7eeff, 0xFFffffff, 0xFFf0f8ff};
                rg = new RadialGradient((left + right) / 2, (top + bottom) / 2, right - left, colors, null, Shader.TileMode.CLAMP);
                paint.setShader(rg);
                rectf = new RectF(left, top, right, bottom);
                //円を描く
                paint.setAlpha(180);        //半透明描写(0で完全透明、255で無透明)
                canvas.drawOval(rectf, paint);
            }
            break;
            case 2: {
                paint.setStyle(Paint.Style.FILL);
              //グラデーション
//                int[] colors = { 0xFF2f4f4f, 0xFF006400, 0xFF66cdaa};
//                int[] colors = { 0xFF2f4f4f, 0xFF002200, 0xFF006400};
                int[] colors = { 0xFF004400, 0xFF002200, 0xFF001100};
                rg = new RadialGradient((left + right) / 2, (top + bottom) / 2, right - left, colors, null, Shader.TileMode.CLAMP);
                paint.setShader(rg);
                rectf = new RectF(left, top, right, bottom);
                //円を描く
                paint.setAlpha(180);        //半透明描写(0で完全透明、255で無透明)
                canvas.drawOval(rectf, paint);
            }
            break;
            case 3: {   //白い貝殻
                paint.setStyle(Paint.Style.FILL);
                //グラデーション
//                int[] colors = { 0xFF2f4f4f, 0xFF006400, 0xFF66cdaa};
//                int[] colors = { 0xFF2f4f4f, 0xFF002200, 0xFF006400};
                int[] colors1 = { 0xFFfafad2, 0xFFffffff, 0xFFfafad2 };
                rg = new RadialGradient((left + right) / 2, (top + bottom) / 2, right - left, colors1, null, Shader.TileMode.CLAMP);
                paint.setShader(rg);
                //パスで描く
                paint.setPathEffect(new CornerPathEffect(50));
                paint.setAlpha(220);        //半透明描写(0で完全透明、255で無透明)
                canvas.drawPath(path, paint);
            }
            break;
            case 4: {   //黄色貝殻
                paint.setStyle(Paint.Style.FILL);
                //グラデーション
//                int[] colors = { 0xFF2f4f4f, 0xFF006400, 0xFF66cdaa};
//                int[] colors = { 0xFF2f4f4f, 0xFF002200, 0xFF006400};
                int[] colors2 = { 0xFFf0e68c, 0xFFf0e68c, 0xFFf0e68c };
                rg = new RadialGradient((left + right) / 2, (top + bottom) / 2, right - left, colors2, null, Shader.TileMode.CLAMP);
                paint.setShader(rg);
                //パスで描く
                paint.setPathEffect(new CornerPathEffect(50));
                paint.setAlpha(220);        //半透明描写(0で完全透明、255で無透明)
                canvas.drawPath(path, paint);
            }
            break;
        }
    }
}

