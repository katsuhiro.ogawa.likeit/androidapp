package com.example.myapp_block_2;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class StartActivity extends AppCompatActivity {
    Button bt1Click, bt2Click, bt3Click;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

    //ナビゲーションバーとステータスバーを非表示にする
        View decor = getWindow().getDecorView();
        // hide navigation bar, show status bar
        decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        // show navigation bar, hide status bar
        decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);

    //ボタンを用意
        bt1Click = findViewById(R.id.stBtn1);
        bt2Click = findViewById(R.id.stBtn2);
        bt3Click = findViewById(R.id.stBtn3);
    //ボタンにリスナーを設定
        StageListener listener = new StageListener();
        bt1Click.setOnClickListener(listener);
        bt2Click.setOnClickListener(listener);
        bt3Click.setOnClickListener(listener);
    //フォントを取得し、ボタンに設定（フォントデータはassetsファイルに入っている）
        Typeface customFont1 = Typeface.createFromAsset(getAssets(), "LibreBaskerville-Regular.ttf");
        bt1Click.setTypeface(customFont1);
        bt2Click.setTypeface(customFont1);
        bt3Click.setTypeface(customFont1);
    }

    private class StageListener implements View.OnClickListener{
        @Override
        public void onClick(View view){
        int id = view.getId();
        switch(id){
            case R.id.stBtn1: {
                Intent intent = new Intent(StartActivity.this, SelectActivity.class);
                startActivity(intent);
                break;
            }

            case R.id.stBtn2: {
                Intent intent = new Intent(StartActivity.this, SettingActivity.class);
                startActivity(intent);
                break;
            }

            case R.id.stBtn3: {

                break;
            }
        }
        }
    }
}